#!/usr/bin/python3

from decouple import config
import datetime

import gspread
from gspread.exceptions import WorksheetNotFound, CellNotFound
from oauth2client.service_account import ServiceAccountCredentials


def insert_data(message, message_type):
    scope = [config('EXPORT_SCOPE')]
    credentials = ServiceAccountCredentials.from_json_keyfile_name(config('CREDS'), scope)

    client = gspread.authorize(credentials)

    sheet = client.open_by_key(config('EXPORT_KEY'))
    try:
        ws = sheet.worksheet(f'{datetime.datetime.now().date()}')
    except WorksheetNotFound:
        ws = sheet.add_worksheet(f'{datetime.datetime.now().date()}',2400 , 7)
        ws.insert_row(['User', 'Start Time', 'End Time', 'Pause Time', 'Continue Time', 'Total Time', 'Comment (optional)'])
    
    if message_type == 'Начало':
            try:
                row = ws.find(message.chat.username).row
                if len(ws.acell(f'C{row}').value) > 0:
                    return 'ended'
                else:
                    return 'not'
            except CellNotFound:
                ws.insert_row([message.chat.username, str(datetime.datetime.now().time())[:-7], '', '', '', '', ''], index=len(ws.get_all_values())+1)
                return True
    elif message_type == 'Конец':
        try:
            row = ws.find(message.chat.username).row
            if ws.acell(f'C{row}').value == '':
                ws.update_acell(f'C{row}', str(datetime.datetime.now().time())[:-7])
                return True
            else:
                return 'isset'
        except CellNotFound:
            return 'not'
    elif message_type == 'Пауза':
        try:
            row = ws.find(message.chat.username).row
            if len(ws.acell(f'C{row}').value) > 0:
                return 'ended'
            pauses = ws.acell(f'D{row}').value.split('\n')
            continues = ws.acell(f'E{row}').value.split('\n')
            if len(pauses) == len(continues):
                if pauses == ['']:
                    ws.update_acell(f'D{row}', str(datetime.datetime.now().time())[:-7])
                    return True
                else:
                    ws.update_acell(f'D{row}', f'{ws.acell(f"D{row}").value}\n{str(datetime.datetime.now().time())[:-7]}')
                    return True
            else:
                return 'not'
        except CellNotFound:
            return 'newday'
    elif message_type == 'Продолжить':
        try:
            row = ws.find(message.chat.username).row
            pauses = ws.acell(f'D{row}').value.split('\n')
            continues = ws.acell(f'E{row}').value.split('\n')
            if len(pauses) == len(continues) + 1 or len(pauses) == len(continues) and continues == ['']:
                if continues == ['']:
                    ws.update_acell(f'E{row}', str(datetime.datetime.now().time())[:-7])
                    return True
                else:
                    ws.update_acell(f'E{row}', f'{ws.acell(f"E{row}").value}\n{str(datetime.datetime.now().time())[:-7]}')
                    return True
            else:
                return 'not'
        except CellNotFound:
            return 'newday'
    elif message_type == 'Comment':
        try:
            row = ws.find(message.chat.username).row
            if len(ws.acell(f'G{row}').value) > 0:
                return 'not'
            else:
                ws.update_acell(f'G{row}', message.text)
                return True
        except CellNotFound:
            pass
    elif message_type == 'Count':
        try:
            row = ws.find(message.chat.username).row
            values = ws.row_values(row)
            print(values)
            if len(values) == 7:
                values = values[:-2]
                if values[-1] == '':
                    values = values[:-2]
            if len(values) == 3:
                start_time = datetime.datetime.strptime(values[1], '%H:%M:%S')
                end_time = datetime.datetime.strptime(values[2], '%H:%M:%S')
                final_time = str(end_time - start_time)
                ws.update_acell(f'F{row}', final_time)
            elif len(values) == 5:
                if len(values[3]) == 8:
                    start_time = datetime.datetime.strptime(values[1], '%H:%M:%S')
                    end_time = datetime.datetime.strptime(values[2], '%H:%M:%S')
                    pause_time = datetime.datetime.strptime(values[3], '%H:%M:%S')
                    continue_time = datetime.datetime.strptime(values[4], '%H:%M:%S')
                    final_time = str(end_time - start_time - (continue_time - pause_time))
                    ws.update_acell(f'F{row}', final_time)
                else:
                    start_time = datetime.datetime.strptime(values[1], '%H:%M:%S')
                    end_time = datetime.datetime.strptime(values[2], '%H:%M:%S')
                    final_time = end_time - start_time
                    pauses = values[3].split('\n')
                    continues = values[4].split('\n')
                    for i in pauses:
                        pause_time = datetime.datetime.strptime(i, '%H:%M:%S')
                        continue_time = datetime.datetime.strptime(continues[pauses.index(i)], '%H:%M:%S')
                        final_time -= (continue_time - pause_time)
                    ws.update_acell(f'F{row}', str(final_time))
                    
        except CellNotFound:
            return 'newday'