import datetime
import os
import telebot
from decouple import config
from export import insert_data

bot = telebot.TeleBot(config('API_TOKEN'))
start_keyboard = telebot.types.ReplyKeyboardMarkup(True, True)
start_keyboard.row('Начало')

first_keyboard = telebot.types.ReplyKeyboardMarkup(True, True)
first_keyboard.row('Конец', 'Пауза', 'Комментарий')

checked_in_keyboard = telebot.types.ReplyKeyboardMarkup(True, True)
checked_in_keyboard.row('Конец', 'Пауза')

pause_keyboard = telebot.types.ReplyKeyboardMarkup(True, True)
pause_keyboard.row('Продолжить')

without_comment = telebot.types.ReplyKeyboardMarkup(True, True)
without_comment.row('Без комментария')

confirm_keyboard = telebot.types.ReplyKeyboardMarkup(True, True)
confirm_keyboard.row('Да', 'Нет')

@bot.message_handler(commands=['start'])
def start_message(message):
	bot.send_message(message.chat.id, f"""
	Привет, {message.chat.first_name}!
	Рады приветсвовать тебя в нашем боте.
	Он поможет тебе мониторить твое рабочее время и даст понять руководству насколько ты добросовестный работник :)
	""", reply_markup=start_keyboard)

@bot.message_handler(content_types=['text'])
def resolve(message):
	if message.text == 'Начало':
		check_in(message)
	elif message.text == 'Конец':
		check_out(message)
	elif message.text == 'Пауза':
		pause(message)
	elif message.text == 'Продолжить':
		Continue(message)
	elif message.text == 'Да' or message.text == 'Нет':
		checkout_confirm(message)
	elif message.text == 'Комментарий':
		open(message.chat.username, 'w').close()
		bot.send_message(message.chat.id, "Введите комментарий который вы хотели бы оставить (например - Работаю дома и т.д.)", reply_markup=without_comment)
	elif message.text == 'Без комментария':
		bot.send_message(message.chat.id, "Вы продолжаете свой рабочий день", reply_markup=checked_in_keyboard)
	else:
		try:
			f = open(message.chat.username, 'r')
			os.remove(message.chat.username)
			comments(message)
		except FileNotFoundError:
			bot.send_message(message.chat.id, 'Прости, я тебя не понимаю :( ')

def check_in(message):
	bot.send_message(message.chat.id, "Подождите, идет обработка данных...")
	res = insert_data(message, 'Начало')
	if res == True:
		bot.send_message(message.chat.id, "Поздравляем с началом рабочего дня!!! Вы можете оставить комментарий", reply_markup=first_keyboard)
	elif res == 'ended':
		bot.send_message(message.chat.id, "Ваш сегодняшний рабочий день уже закончен", reply_markup=start_keyboard)
	elif res == 'not':
		bot.send_message(message.chat.id, "Вы уже начали сегодняшний рабочий день")

def check_out(message):
	bot.send_message(message.chat.id, "Вы уверены что хотите закончить рабочий день? ", reply_markup=confirm_keyboard)

def pause(message):
	bot.send_message(message.chat.id, "Подождите, идет обработка данных...")	
	res = insert_data(message, 'Пауза')
	if res == True:
		bot.send_message(message.chat.id, "Ваше рабочее время поставлено на паузу", reply_markup=pause_keyboard)
	elif res == 'ended':
		bot.send_message(message.chat.id, "Ваш сегодняшний рабочий день уже закончен", reply_markup=start_keyboard)
	elif res == 'newday':
		bot.send_message(message.chat.id, "Вы еще не начали сегодняшний рабочий день", reply_markup=start_keyboard)
	else:
		bot.send_message(message.chat.id, "Вы уже поставили свое время на паузу", reply_markup=pause_keyboard)

def Continue(message):
	bot.send_message(message.chat.id, "Подождите, идет обработка данных...")		
	if insert_data(message, 'Продолжить') == True:
		bot.send_message(message.chat.id, "Вы продолжаете работать", reply_markup=checked_in_keyboard)
	elif res == 'newday':
		bot.send_message(message.chat.id, "Вы еще не начали сегодняшний рабочий день", reply_markup=start_keyboard)
	else:
		bot.send_message(message.chat.id, "Вы не поставили свое время на паузу или уже закончили ее", reply_markup=checked_in_keyboard)

def comments(message):
	bot.send_message(message.chat.id, "Подождите, идет обработка данных...")
	if insert_data(message, 'Comment') == True:
		return bot.send_message(message.chat.id, 'Ваш комментарий успешно записан!', reply_markup=checked_in_keyboard)
	else:
		return bot.send_message(message.chat.id, "Вы уже оставляли сегодня комментарий")

def checkout_confirm(message):
	if message.text == 'Да':
		res = insert_data(message, 'Конец')
		bot.send_message(message.chat.id, 'Поздравяем с окончанием рабочего дня!!', reply_markup=start_keyboard)
		insert_data(message, 'Count')
	elif message.text == 'Нет':
		bot.send_message(message.chat.id, 'Вы продолжаете работать', reply_markup=checked_in_keyboard)
bot.polling()
